#pragma once
#include <cstddef>

template<std::size_t size>
struct permutations {
	static_assert(2<=size && size<=8,"lookup table not computed");
	
	static const unsigned char values[][size];
	static const std::size_t count;
};

/* template specialization has to go in permutations.cpp, see http://stackoverflow.com/a/4445772 */