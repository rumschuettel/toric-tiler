.PHONY: all, clean
CXXFLAGS += -Wall -Wno-enum-compare -pthread -std=c++11 -Ofast -march=native
BOOST_MODULES = program_options

LDFLAGS += $(addprefix -lboost_,$(BOOST_MODULES))

SOURCES = main.cpp permutations.cpp utils.cpp import.cpp
HEADERS = $(wildcard %.h)
OBJECTS = $(SOURCES:%.cpp=%.o)

PROGNAME := tiler

all: $(PROGNAME)

$(PROGNAME): $(OBJECTS)
	$(CXX) $(CXXFLAGS) -o $@ $(OBJECTS) $(LDFLAGS)

clean:
	@echo "Clean."
	-rm -f *.o $(PROGNAME)
