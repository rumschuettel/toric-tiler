#pragma once

#include <vector>
std::vector<std::vector<unsigned int>> Import(const char* filename);

// this class is not performance-critical
class Tile {
public:
	typedef char ColorT;
	typedef std::vector<Tile> ListT;

public:
	ColorT top, right, bottom, left;

	Tile();
	Tile(ColorT top, ColorT right, ColorT bottom, ColorT left);

	bool operator==(const Tile &other) const;

	static ListT FromNumbers(const std::vector<unsigned int> &numbers);
};
