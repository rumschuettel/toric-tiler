#include "utils.h"

std::size_t factorial(std::size_t n)
{
	std::size_t ret = 1;
	for (std::size_t i = 1; i < n; ret *= i, i++);
	return ret;
}

std::size_t binomial(std::size_t n, std::size_t k)
{
	return factorial(n)/factorial(k)/factorial(n-k);
}
