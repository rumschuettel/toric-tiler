// main.cpp : Defines the entry point for the console application.
//

#include "import.h"
#include "generators.h"
#include "tiling.h"

#include <iostream>
#include <string>
#include <sstream>

#include <future>
#include <chrono>

class AsyncLimited {
protected:
	std::vector<std::future<void>> futures;
	std::vector<std::function<void()>> tasks;
	std::size_t running;
	std::mutex mut;
	
	bool launch()
	{
		if (tasks.empty() || running >= Limit())
			return false;
		
		running++;
		std::function<void()> task = tasks.back();
		tasks.pop_back();
		
		futures.push_back(std::async(std::launch::async, [this, task]() {
			task();
			this->running--;
		}));
		
		return true;
	}
	
public:
	AsyncLimited(std::vector<std::function<void()>> tasks)
		: tasks(tasks), running(0)
	{
		std::async(std::launch::deferred, [this]() {
			while (!this->tasks.empty()) {
				while (this->launch());
				std::this_thread::sleep_for(std::chrono::seconds(1));
			}
			
			for (auto &f : futures)
				f.get();
		}).get();
	}
	
	static std::size_t Limit()
	{
		static std::size_t hwlimit = std::thread::hardware_concurrency();
		return hwlimit > 0 ? hwlimit : 4; // I'm just counting on HT here, no idea how to detect that
	}
};


// output some timing information
void timing(std::size_t left)
{
	// avoid race conditions
	static std::size_t lastleft = left;
	if (left == lastleft)
	  return;
  
	using namespace std::chrono;
	static auto last = system_clock::now();
	auto now = system_clock::now();
	duration<float, std::milli> diff = now - last;

	if (diff.count() == 0)
		return;

	auto rate = (float)(lastleft - left) / diff.count() * 1000;
	static auto srate = 0.0;
	srate = (srate * 199 + rate) / 200;

	std::cout << srate << " op/s, " << (int)(left / srate) << " s left    \r";
	last = now;
	lastleft = left;
}

#include <boost/program_options.hpp>
#include <mutex>
#include <cstdio>
#include <sys/resource.h>
bool setStack(const rlim64_t stackSizeMB)
{
	auto stackSize = stackSizeMB * 1024L * 1024L;
    struct rlimit rl;
    int result;

    result = getrlimit(RLIMIT_STACK, &rl);
    if (result == 0) {
        if (rl.rlim_cur < stackSize) {
            rl.rlim_cur = stackSize;
            result = setrlimit(RLIMIT_STACK, &rl);
            if (result != 0) {
				std::cerr << "setrlimit returned " << result << std::endl;
				return false;
            }
        }
	} else {
		std::cerr << "getrlimit returned " << result << std::endl;
		return false;
	}
	
	return true;
}

int main(int argc, char* argv[])
{
	if (!setStack(48))
		return 1;
	
	// parse command line
	namespace po = boost::program_options;
	std::size_t rskip, rcount;
	bool verbose, noprompt, nocheck;
	
	po::options_description desc("Tiler options");
	desc.add_options()
		("help,h", "show help message")
		("rskip", po::value<std::size_t>(&rskip)->default_value(0), "R matrices to skip. 0 means from the start.")
		("rcount", po::value<std::size_t>(&rcount)->default_value(0), "R matrices to process. 0 means all.")
		("verbose", po::bool_switch(&verbose)->default_value(false), "show verbose progress output.")
		("noprompt", po::bool_switch(&noprompt)->default_value(false), "do not prompt at end.")
		("nocheck", po::bool_switch(&nocheck)->default_value(false), "do not check range parameters. You HAVE TO pass rcount and rskip")
	;
	
	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);
	
	if (vm.count("help")) {
 	   std::cout << desc << "\n";
    	return 1;
	}

	// number of intermediate tiles we allow and maximum iterations
	const unsigned int maxTiles = 1<<8, maxIterations = 100;
	typedef Tiling<maxTiles, maxIterations> Tiling;
	/*
	startTiles: number of tiles we start with
	minSparsity: minimum allowed neighbours of a tile in the x and y direction
	maxSparsity: maximum ...
	we can adjust these parameters independent for x and y, try your luck, maybe you find good candidates
	*/
	const std::size_t plaquettes = 5, stars = 5;
	/*
	Step() iterates over all matrices with these properties and the given sparsity.
	StepEquivalent() skips all that are similar under adjunction of a symmetric matrix.
	*/
	typedef ValidMatrices<7, 7, 1, 6> MatT;
	
	// print how much we have to check and which range we picked
	std::size_t total = rcount;
	if (!nocheck) {
		std::size_t rtotal = MatT::EquivalentCandidates(), utotal = MatT::Candidates();
		if (rcount == 0 || rcount > rtotal)
			rcount = rtotal;

		total = rcount * utotal;
		std::cout << "checking " << rcount << " out of " << rtotal << " Rs x " << utotal << " Us = " << total << std::endl;
	} else
		std::cout << "checking " << rcount << " Rs (run with --nocheck)" << std::endl;
		
	std::cout << "starting at R " << rskip << std::endl;
	std::cout << "limiting to " << AsyncLimited::Limit() << " threads." << std::endl;


	// start tiling
	std::size_t blowsup = 0, indeterminate = 0, impossible = 0, left = total;
	std::size_t best = 0;

	MatT Rs;
	
	std::vector<std::future<void>> futures;
	std::vector<std::function<void()>> tasks;
	std::mutex mtx;

    while (Rs.StepEquivalent() && rcount-- > 0) {
		// skip first rskip Rs
		for (; rskip > 0; rskip --)
			Rs.StepEquivalent();
		
        tasks.push_back([&, Rs, rcount]() {
            Tiling tiling;
            MatT Us;
            do {
                left--;

				if (!tiling.FromLambda<false>(Rs, Us, MatT::SIZE))
					continue;
				
                Tiling::Outcome outcome = tiling.Launch(MatT::SIZE);
                switch (outcome.result) {
                case Tiling::BlowsUp:
                    blowsup++;
                    break;
                case Tiling::Indeterminate:
                    indeterminate++;
                    break;
                case Tiling::Impossible:
                    mtx.lock(); // synchronize output
                    if (outcome.depth > best) {
                        best = outcome.depth;
                        std::cout << (1 << best) << " (" << best << " steps): @ " << left << "              " << std::endl;
                        std::cout << static_cast<std::string>(Rs) << static_cast<std::string>(Us);
                        std::cout.flush();
                    }
                    mtx.unlock();
                    impossible++;
                    break;
                }

                // status
                if (verbose && (left % 1000) == 0) {
                    mtx.lock();
					timing(left);
                    mtx.unlock();
                }
            } while (Us.Step());
			
			mtx.lock();
			std::cout << "finished @ R " << rcount << "           " << std::endl;
			mtx.unlock();
        });

    };

    AsyncLimited al(tasks);


	if (!noprompt) {
		std::cout << std::endl << "Press any key to quit." << std::endl;
		std::getchar();
	} else {
		std::cout << "done." << std::endl;
	}
    return 0;
}

