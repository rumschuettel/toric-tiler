#pragma once

#include "permutations.h"
#include "utils.h"
#include <array>
#include <memory>

template<std::size_t sizeA, std::size_t sizeB>
struct product_permutations {
	typedef permutations<sizeA> pA;
	typedef permutations<sizeB> pB;
	enum {
		SIZE = sizeA + sizeB
	};
	enum {
		COUNT = Factorial<sizeA>::value*Factorial<sizeB>::value
	};
	typedef unsigned char PermT[SIZE];
	
	// heap
	//std::shared_ptr<std::array<PermT, COUNT>> values;
	// stack
	std::array<PermT, COUNT> values;
	
	product_permutations()
//		: values(new std::array<PermT, COUNT>)
	{
		std::size_t index = 0;

		// products
		for (std::size_t p = 0; p < pA::count; p++) {
			for (std::size_t q = 0; q < pB::count; q++) {
				PermT &n = (values)[index++];
				std::size_t i;
				for (i = 0; i < sizeA; i++)
					n[i] = pA::values[p][i];
				for (; i < SIZE; i++)
					n[i] = static_cast<unsigned char>(pB::values[q][i-sizeA] + sizeA);
			}
		}
	}
	
	static const product_permutations<sizeA, sizeB>& I()
	{
		static const product_permutations<sizeA, sizeB> instance;
		return instance;
	}
};
