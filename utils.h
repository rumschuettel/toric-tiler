#pragma once
#include <cstddef>

// can't wait for constexpr
#define imin(a, b) (((a) < (b)) ? (a) : (b))
#define imax(a, b) (((a) > (b)) ? (a) : (b))
std::size_t factorial(std::size_t n);
std::size_t binomial(std::size_t n, std::size_t k);

template <int N>
struct Factorial 
{
    enum { value = N * Factorial<N - 1>::value };
};

template <>
struct Factorial<0> 
{
    enum { value = 1 };
};


