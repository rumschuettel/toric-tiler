#include "import.h"

#include <iostream>
#include <sstream>
#include <fstream>
#include <iterator>

std::vector<std::vector<unsigned int>> Import(const char* filename)
{
	std::vector<std::vector<unsigned int>> numbers;
	std::ifstream file(filename);

	if (!file.is_open()) {
		std::cout << "error reading " << filename << std::endl;
		return{ {} };
	}

	for (std::string line; std::getline(file, line);) {
		std::istringstream buffer(line);
		std::vector<unsigned int> tiles{
			std::istream_iterator<unsigned int>(buffer),
			std::istream_iterator<unsigned int>()
		};

		numbers.push_back(tiles);
	}

	return numbers;
}

// this class is not performance-critical
Tile::Tile()
{}
	
Tile::Tile(ColorT top, ColorT right, ColorT bottom, ColorT left)
	: top(top), right(right), bottom(bottom), left(left)
{}

bool Tile::operator==(const Tile &other) const
{
	return (top == other.top) && (right == other.right) && (bottom == other.bottom) && (left == other.left);
}

Tile::ListT Tile::FromNumbers(const std::vector<unsigned int> &numbers)
{
	ListT set(numbers.size());

	std::size_t index = 0;
	for (unsigned int number : numbers) {
		number--; //mathematica starts indices with 1. The following only works for integral colors.
		auto bit = [=](unsigned int b, unsigned int p) {
			return (number & ((1 << 7) >> b)) > 0 ? (1 << p) : 0;
		};

		set[index++] = {
			static_cast<Tile::ColorT>(bit(0, 2) | bit(1, 1) | bit(2, 0)),
			static_cast<Tile::ColorT>(bit(2, 2) | bit(3, 1) | bit(4, 0)),
			static_cast<Tile::ColorT>(bit(6, 2) | bit(5, 1) | bit(4, 0)),
			static_cast<Tile::ColorT>(bit(0, 2) | bit(7, 1) | bit(6, 0))
		};
	}

	return set;
}
