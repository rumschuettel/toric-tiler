#include "backtrack-tiling.h"

#include <iostream>

int main(int argc, char* argv[])
{

	const unsigned int maxLength = 16;
	const unsigned int tilesNum = 6;

	typedef BacktrackTiling<maxLength, tilesNum> BTiling;
	
	const BTiling::MatrixT R = {
					{0, 0, 0, 0, 0, 1}, //0
				    {0, 0, 1, 0, 1, 0},
				    {0, 1, 0, 1, 0, 0},
				    {1, 0, 0, 0, 0, 0},
				    {1, 0, 0, 0, 0, 0},
				    {0, 0, 1, 0, 1, 0} 
				};
	const BTiling::MatrixT U = {
					{1, 0, 1, 0, 0, 1},
				    {0, 0, 0, 1, 0, 0},
				    {1, 0, 1, 0, 0, 1},
				    {0, 1, 0, 0, 1, 0},
				    {0, 0, 0, 1, 0, 0},
				    {1, 0, 1, 0, 0, 1} 
				};

	BTiling bt;
	bt.FromMatrices(R,U);
	auto out = bt.Launch();

	std::cout << "Length: " << out  << std::endl;

    return 0;
}

