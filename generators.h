#pragma once

#include "permutations.h"
#include "product_permutations.h"
#include "utils.h"

#include <string>
#include <sstream>
#include <iostream>
#include <cassert>
#include <algorithm>

template<typename T, std::size_t sizex, std::size_t sizey>
class ToStringMixin : public T {
public:
	operator std::string() const {
		std::stringstream out;
		for (std::size_t y = 0; y < sizey; y++) {
			for (std::size_t x = 0; x < sizex; x++)
				out << (*this)(x, y) << " ";
			out << std::endl;
		}

		return out.str();
	}

};

template <std::size_t sizex, std::size_t sizey, std::size_t minSparsity, std::size_t maxSparsity, typename BitT>
class ValidMatricesBase {
	static_assert(minSparsity <= maxSparsity && maxSparsity <= sizex, "wrong matrix parameters");
	static_assert(sizex > 1 && sizey > 1, "size too small, not a matrix");
	static_assert(sizex <= sizeof(BitT) * 8, "bit type BitT too small");

protected:
	typedef ValidMatricesBase<sizex, sizey, minSparsity, maxSparsity, BitT> MatrixT;
	BitT instance[sizey];

	static const BitT MIN_HAMMING = (1LL << minSparsity) - 1;
	static inline BitT NextHamming(const BitT v)
	{
		BitT t = (v | (v - 1)) + 1;
		return t | ((((t & -t) / (v & -v)) >> 1) - 1);
	}
	static inline BitT DistributeBits(const BitT o, const BitT mask)
	{
		BitT r = 0;
		for (std::size_t i = 0, j = 0; i < sizex; i++) {
			if (((1LL << i) & mask) == 0) {
				r |= ((1LL << j) & o) << (i - j);
				j ++;
			}
		}
		return r;
	}

	// c++ doesn't allow template member template partial specialization,
	// so this has to be an object. The complier optimizes the overhead away.
	template <std::size_t depth, typename DUMMY = void>
	struct Next	{
		static_assert(depth <= 32, "template recursion too deep");

		Next<depth - 1> next;
		BitT (&instance)[sizey];
		
		std::size_t maskWeight;
		std::size_t unmaskedWeight;
		
		std::size_t assumedRow;
		bool overshot;
		
		BitT unmasked;
		BitT mask;
		
		Next(BitT (&instance)[sizey])
			: next(instance), instance(instance), maskWeight(minSparsity), unmaskedWeight(minSparsity),
			  assumedRow(0), overshot(false)
		{
			// initialize to ...000111 where the number of 1s is given by minSparsity
			unmasked = mask = instance[depth] = MIN_HAMMING;
		}	

		inline bool operator()() {
			// assume one of the previous distinct rows
			while (assumedRow < depth-1) {
				assumedRow++;
				BitT candidate = instance[assumedRow];
				
				// check if candidate row is unique
				std::size_t i;
				for (i = 0; i < assumedRow; i++) {
					if (instance[i] == candidate)
						break;
				}
				if (i != assumedRow)
					continue;
				
				instance[depth] = candidate;
				mask = next.mask; // nothing new since we're reusing a previous row
				maskWeight = next.maskWeight;
				return true;
			}
			
			std::size_t weightLeft = sizex - next.maskWeight;
			if (weightLeft < minSparsity || overshot) {
				overshot = false;
				assumedRow = 0;

				// increment previous rows and reset this row
				bool nextPossible = next();
				instance[depth] = instance[0];
				mask = next.mask;
				maskWeight = next.maskWeight;
				return nextPossible;
			}

			// cycle through rows that are orthogonal to all previous ones
			// if next.mask == 0010001, we create
			// 0000010, 0000100, 0000110, ..., 0001110, 0100000, ..., 0101110, ..., 1101110
			BitT v = DistributeBits(unmasked, next.mask);
			mask = next.mask | v;
			maskWeight = next.maskWeight + unmaskedWeight;
			instance[depth] = v;

			// increment unmasked
			unmasked = NextHamming(unmasked);
			if (unmasked >= 1LL << weightLeft) { // overshot
				unmaskedWeight ++;
				// unary increment
				if (unmaskedWeight > imin(maxSparsity, weightLeft)) {
					overshot = true;
					unmasked = MIN_HAMMING;
					unmaskedWeight = minSparsity;
				} else {
					overshot = false;
					unmasked = (1LL << unmaskedWeight) - 1;
				}
			}
			
			return true;			
		}
	};
	template <std::size_t depth, typename DUMMY>
	friend struct Next;

	template <typename DUMMY>
	struct Next<0, DUMMY> {
		BitT (&instance)[sizey];
		std::size_t maskWeight;
		BitT &mask;

		Next(BitT (&instance)[sizey])
			: instance(instance), maskWeight(minSparsity), mask(instance[0])
		{
			instance[0] = MIN_HAMMING;
		}

		inline bool operator()() {
			BitT v = instance[0];

			// hamming increment
			v = NextHamming(instance[0]);
			if (v >= 1LL << sizex) { // overshot
				v = instance[0];
				while (!(v & 1)) v >>= 1;

				// unary increment
				v = v | (v + 1);
				if (++maskWeight > maxSparsity) { // overshot
					instance[0] = MIN_HAMMING; // reset? we might not want to
					maskWeight = minSparsity;
					return false;
				}
			}

			instance[0] = v;
			return true;
		}
	};

	Next<sizey-1> next;

	typedef std::size_t HashT;
	HashT hash() const
	{
		static_assert(sizex * sizey <= sizeof(HashT) * 8, "hash type HashT too small");
		
		HashT hash = 0;
		for (std::size_t i = 0; i < sizex; i++)
			for (std::size_t j = 0; j < sizey; j++)
				hash |= (*this)(i, j) << (sizey*i + j);
		return hash;
	}
public:
	static inline HashT hashTranspose(HashT in)
	{
		HashT out = 0;
		for (std::size_t j = 0; j < sizey; j++)
			for (std::size_t i = 0; i < sizex; i++) {
				std::size_t offset = sizey*i + j;
				out |= ((in & (1LL << offset)) >> offset) << (SIZE-1 - sizex*j - i);
			}
		return out;
	}

	bool operator<(const MatrixT& other) const
	{
		static_assert(sizex == sizey, "no symmetries for non-square matrices");
		typedef permutations<SIZE> Equivalents;
		HashT you = other.hash();

		for (std::size_t p = 0; p < Equivalents::count; p++) {
			HashT me = 0;
			std::size_t i = 0;
			for (const std::size_t si : Equivalents::values[p]) {
				std::size_t j = 0;
				for (const std::size_t sj : Equivalents::values[p]) {
					me |= (*this)(si, sj) << (SIZE*i + j);
					j++;
				}
				i++;
			}
			
			if (me < you)
				return true;
		}

		return false;
	}

public:
	enum {
		SIZEX = sizex,
		SIZEY = sizey
	};
	enum {
		SIZE = sizex
	};
	
	template <typename GenA, typename GenB>
	friend class SupertileMatricesBase;
	
	ValidMatricesBase()
		: next(instance)
	{}

	inline bool Step()
	{
		return next();
	}

	bool StepEquivalent()
	{
		do {
			if (!Step())
				return false;
		} while ((*this) < (*this));
		return true;
	}

	inline bool operator()(std::size_t x, std::size_t y) const
	{
		assert(x < sizex && y < sizey);

		return !!(instance[y] & (1LL << x));
	}

	static std::size_t Candidates()
	{
		std::size_t candidates = 0;
		for (MatrixT mat; mat.Step(); ++candidates);
		return candidates;
	}
	
	static std::size_t EquivalentCandidates()
	{
		std::size_t candidates = 0;
		for (MatrixT mat; mat.StepEquivalent(); ++candidates);
		return candidates;
	}
};

template <std::size_t sizex, std::size_t sizey, std::size_t minSparsity, std::size_t maxSparsity>
using ValidMatrices = ToStringMixin<
	ValidMatricesBase<sizex, sizey, minSparsity, maxSparsity, long long>,
	sizex, sizey
>;

template <typename GenA, typename GenB>
class SupertileMatricesBase {
	static_assert(GenA::SIZEX == GenB::SIZEY && GenA::SIZEY == GenB::SIZEX, "invalid part matrices given");

	typedef SupertileMatricesBase<GenA, GenB> MatrixT;

protected:
	GenA genA;
	GenB genB;

	inline bool mirrorSymmetry() const
	{
		return genA.hash() < GenB::hashTranspose(genB.hash());
	}
	
	typedef product_permutations<GenA::SIZEX, GenB::SIZEX> Equivalents;
	bool operator<(const MatrixT& other) const
	{
		auto you_ha = other.genA.hash();
		auto you_hb = other.genB.hash();
		auto &values = (Equivalents::I().values);

		for (std::size_t p = 0; p < Equivalents::I().COUNT; p++) {
			typename GenA::HashT ha = 0;
			typename GenB::HashT hb = 0;
			std::size_t y = 0;
			for (const std::size_t sy : values[p]) {
				std::size_t x = 0;
				for (const std::size_t sx : values[p]) {
					// we have to match the hash structure
					if ((x < GenA::SIZEX && y < GenB::SIZEY) || (x >= GenA::SIZEX && y >= GenB::SIZEY)) {
						x++;
						continue;
					}

					if (x < GenA::SIZEX)
						ha |= genA(sx, sy - GenB::SIZEY) << (GenA::SIZEX*(y-GenB::SIZEY) + x);
					else
						hb |= genB(sx - GenA::SIZEX, sy) << (GenB::SIZEX*y + x-GenA::SIZEX);
					x++;
				}
				y++;
			}
			
			if (ha < you_ha)
				return true;
			if (ha == you_ha && hb < you_hb)
				return true;
		}

		return false;
	}
	
public:
	enum {
		SIZE = GenA::SIZEX + GenB::SIZEX
	};

	inline bool Step()
	{
		// handle mirror symmetry for all matrices
		do {
			if (!genA.Step())
				if (!genB.Step())
					return false;
		} while (mirrorSymmetry());
		return true;
	}

	bool StepEquivalent()
	{
		do {
			if (!Step())
				return false;
		} while ((*this) < (*this));
		return true;
	}

	inline bool operator()(std::size_t x, std::size_t y) const
	{
		assert(x < SIZE && y < SIZE);

		if ((x < GenA::SIZEX && y < GenB::SIZEY) || (x >= GenA::SIZEX && y >= GenB::SIZEY))
			return 0;
		if (x < GenA::SIZEX)
			return genA(x, y-GenB::SIZEY);
		return genB(x-GenA::SIZEX, y);
	}

	static std::size_t Candidates()
	{
		std::size_t candidates = 0;
		for (MatrixT mat; mat.Step(); candidates++);
		return candidates;
	}

	static std::size_t EquivalentCandidates()
	{
		std::size_t candidates = 0;
		for (MatrixT mat; mat.StepEquivalent(); candidates++);
		return candidates;
	}
};


template <typename GenA, typename GenB>
using SupertileMatrices = ToStringMixin<
	SupertileMatricesBase<GenA, GenB>,
	GenA::SIZEX + GenB::SIZEX, GenA::SIZEY + GenB::SIZEY
>;
