#pragma once
#include <functional>
#include <cassert>


// memory alignment might make a difference
template<std::size_t maxTiles, std::size_t maxIterations, typename BoolT = bool>
class Tiling {
	static_assert(sizeof(BoolT) * maxTiles * maxTiles <= (1 << 30) /* 1 GB */, "too little memory");

public:
	enum Result {
		Impossible,
		Indeterminate,
		BlowsUp
	};
	struct Outcome {
		Result result;
		std::size_t depth;
		std::size_t candidates;
	};
	typedef BoolT MatrixT[maxTiles][maxTiles];
	typedef std::size_t IndexT[maxTiles];

protected:
	MatrixT rightA, rightB, upA, upB;
	IndexT rightLoopI, upLoopI;
	
	struct Arr2x2 {
		std::size_t tl, tr, bl, br;
	} possible2x2[maxTiles];

	// calculate the right and up matrices from a list of tiles.
	// this is used when we import the tiles from mathematica or somewhere else
	// where we actually get tiles.
	void RightUpM(MatrixT &right, MatrixT &up, const Tile::ListT &tiles)
	{
		assert(tiles.size() <= maxTiles);
		std::size_t i = 0;
		for (const Tile &a : tiles) {
			std::size_t j = 0;
			for (const Tile &b : tiles) {
				right[i][j] = (a.right == b.left);
				up[i][j] = (a.top == b.bottom);
				j++;
			}
			i++;
		}
	}
	// calculate right and up matrices from a list of allowed 2x2 tiles
	// and the old transition tables. Returns true if an obviously periodic tile is found
	bool RightUpM(MatrixT &newright, MatrixT &newup,
					  const MatrixT &right, const MatrixT &up,
					  const Arr2x2(&arrays)[maxTiles], const std::size_t count)
	{
		assert(count <= maxTiles);
		
		std::size_t r = 0, u = 0;
		
		for (std::size_t i = 0; i < count; i++) {
			const Arr2x2 &a = arrays[i];
			for (std::size_t j = 0; j < count; j++) {
				const Arr2x2  &b = arrays[j];
				newright[i][j] = right[a.tr][b.tl] && right[a.br][b.bl];
				newup[i][j] = up[a.tl][b.bl] && up[a.tr][b.br];
			}
			
			// collect self-looping tiles
			if (newright[i][i])
				rightLoopI[r++] = i;
			if (newup[i][i])
				upLoopI[u++] = i;
			
			if (newright[i][i] && newup[i][i])
				return false;
		}
		
		std::size_t i, j;
		auto checkTriple = [&](MatrixT &matrix, IndexT &index) -> bool {
			if (matrix[index[i]][index[j]]) {
				// A B A B, where A goes on top of A, B likewise
				if (matrix[index[j]][index[i]])
					return false;
				
				for (std::size_t k = 0; k < u; k++) {
					// A B C A B C, where A goes on top of A, B, C likewise
					if (matrix[index[j]][index[k]] && matrix[index[k]][index[i]])
						return false;
				}
			}	
			
			return true;
		};
		
		for (i = 0; i < u; i++) {
			for (j = 0; j < u; j++) {
				if (!checkTriple(newright, upLoopI))
					return false;
				if (!checkTriple(newup, rightLoopI))
					return false;
			}
		}
		
		

		return true;
	}

	std::size_t Iterate(const MatrixT &right, const MatrixT &up,
						MatrixT &newright, MatrixT &newup, const std::size_t tiles)
	{
		assert(tiles <= maxTiles);

		// check if we can fill the 2d array
		// k l
		// i j
		// indexed in possible2x2 as {k, l, i, j}.
		std::size_t candidates = 0;
		for (std::size_t i = 0; i < tiles; i++)
			for (std::size_t j = 0; j < tiles; j++) {
				if (!right[i][j]) continue;
				for (std::size_t k = 0; k < tiles; k++) {
					if (!up[i][k]) continue;
					for (std::size_t l = 0; l < tiles; l++) {
						if (!up[j][l] || !right[k][l]) continue;
						if (candidates == maxTiles)
							return candidates + 1;

						possible2x2[candidates++] = { k, l, i, j };
					}
				}
			}

		if (candidates == 0)
			return 0;

		if (!RightUpM(newright, newup, right, up, possible2x2, candidates))
			return maxTiles + 1; // or some distinct value signifying periodicity
		return candidates;
	}

public:
	void FromTiles(const Tile::ListT &tiles)
	{
		assert(tiles.size() <= maxTiles);
		RightUpM(rightA, upA, tiles);
	}

	template<bool supertileChecks = true>
	bool FromLambda(
		const std::function<bool(std::size_t, std::size_t)> &R,
		const std::function<bool(std::size_t, std::size_t)> &U,
		std::size_t count)
	{
		assert(count <= maxTiles);

		for (std::size_t i = 0; i < count; i++)
			for (std::size_t j = 0; j < count; j++) {
				rightA[i][j] = R(i, j);
				upA[i][j] = U(i, j);
			}
			
		if (!supertileChecks)
			return true;
		
		// alterning condition
		for (std::size_t i = 0; i < count; i++)
			for (std::size_t j = i+1; j < count; j++) {
				if ((rightA[i][j] && rightA[j][i]) != (upA[i][j] && upA[j][i]))
					return false;
			}
		// triple condition 1:
		// if A B C and A B' C and B ^ T, then B' ^ T
		for (std::size_t i = 0; i < count; i++)
			for (std::size_t j = 0; j < count; j++) {
				long lastHorizontal = -1, lastVertical = -1;
				for (std::size_t k = 0; k < count; k++) {
					if (rightA[i][k] && rightA[k][j]) {
						if (lastHorizontal >= 0) {
							// row and column of U matrix have to match
							for (std::size_t l = 0; l < count; l++)
								if ((upA[k][l] != upA[lastHorizontal][l]) || (upA[l][k] != upA[l][lastHorizontal]))
									return false;
						}
						lastHorizontal = (long)k;
					}
					if (upA[i][k] && upA[k][j]) {
						if (lastVertical >= 0) {
							// row and column of U matrix have to match
							for (std::size_t l = 0; l < count; l++)
								if ((rightA[k][l] != rightA[lastVertical][l]) || (rightA[l][k] != rightA[l][lastVertical]))
									return false;
						}
						lastVertical = (long)k;
					}
				}
			}
			
		// TL forbidden/allowed constraint
		for (std::size_t i = 0; i < count; i++)
			for (std::size_t j = 0; j < count; j++)
				for (std::size_t k = 0; k < count; k++)
					for (std::size_t l = 0; l < count; l++)
						if (upA[i][j] && rightA[j][k] && upA[l][k] && !rightA[i][l])
							return false;

		return true;
	}


	Outcome Launch(const std::size_t count)
	{
		assert(count <= maxTiles);

		std::size_t depth = 0, candidates = count;
		for (; depth <= maxIterations && candidates > 0 && candidates <= maxTiles; depth++) {
			candidates = !(depth % 2) ?
				Iterate(rightA, upA, rightB, upB, candidates) :
				Iterate(rightB, upB, rightA, upA, candidates);
		}

		if (candidates == 0)
			return{ Impossible, depth - 1, 0 };
		else if (candidates > maxTiles)
			return{ BlowsUp, depth - 1, candidates };
		else // depth > maxIterations
			return{ Indeterminate, depth - 1, candidates };
	}
};
