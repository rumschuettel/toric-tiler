#pragma once
#include <functional>
#include <cassert>
#include <iostream>
#include <sstream>

template<std::size_t maxLength, std::size_t tilesNum>
class BacktrackTiling {

public:
    typedef bool MatrixT[tilesNum][tilesNum];
    typedef int TilingT[maxLength][maxLength];
    struct Outcome {
	int length;
	TilingT tiling;
    } out;

protected:
    MatrixT right, up;

    // return the size of the biggest square that can be tiled, or -1 otherwise
    int Iterate(const std::size_t &x, const std::size_t &y, TilingT &tiling)
    {
	if (x >= maxLength || y >= maxLength)
	    return -1;
	  
	int best = -1;
	int cur = -1;
	  
	// let's follow an up-then-right pattern to grow the tiling
	auto iterative_step = [&] (std::size_t i){
	    int next_x, next_y;
	    if (x == 0 && y == 0){
	      
		next_x = 1;
		next_y = 0;
	      
	    } else if (y < x){
		if (y == 0  && !right[ tiling[x-1][0] ][i])
		    return -1;
		if (y > 0 && (!right[ tiling[x-1][y] ][i] || !up[ tiling[x][y-1] ][i]))
		    return -1;
	      
		next_x = x;
		next_y = y+1;
	      
	    } else { // y >= x
		if (y == x && !up[ tiling[x][y-1] ][i])
		    return -1;
		if (y > x && (!right[i][ tiling[x+1][y] ] || !up[ tiling[x][y-1] ][i]))
		    return -1;
	      
		if (x > 0){
		    next_x = x-1;
		    next_y = y;
		} else {
		    next_x = y+1;
		    next_y = 0;
		}
	    }

	    tiling[x][y] = i;
	    return Iterate(next_x, next_y, tiling);
	};

	for (std::size_t i = 0; i < tilesNum; i++){
	    // std::cout << "trying tile " << i << " at (" << x << ", " << y << ")" << std::endl;
	    cur = iterative_step(i);	  
	    if (x == 0 && y != 0){
		/* since we do have completed a square, in case
		 * the tiling cannot be extended, we can still return
		 * the current length as a valid tiling-size.
		 */
		cur = (cur < 0) ? y : cur;
	    }
	    if (cur > best){
		best = cur;
		tiling[x][y] = i;
	    }
	    if (best == maxLength-1) // urray!
		return best;
	}
	return best;
    }

public:
    void FromLambda(
		    const std::function<bool(std::size_t, std::size_t)> &R,
		    const std::function<bool(std::size_t, std::size_t)> &U
		    )
    {
	for (std::size_t i = 0; i < tilesNum; i++)
	    for (std::size_t j = 0; j < tilesNum; j++) {
		right[i][j] = R(j, i);
		up[i][j] = U(i, j);
	    }
    }

    void FromMatrices(const MatrixT &R, const MatrixT &U)
    {
	for (std::size_t i = 0; i < tilesNum; i++)
	    for (std::size_t j = 0; j < tilesNum; j++) {
		right[i][j] = R[i][j];
		up[i][j] = U[i][j];
	    }
    }

    std::ostringstream repr(const TilingT &tiling, const int max){	       
	std::ostringstream stream;
	stream.str("");
	for (std::size_t j = 0; j <= max; j++){
	    for (std::size_t i = 0; i <= max; i++)
		stream << tiling[i][max-j] << " ";
	    stream << std::endl;
	}
	return stream;
    }

    int Launch()
    {
	// the starting point
	TilingT tiling;
	for (std::size_t i = 0; i < maxLength; i++)
	    for (std::size_t j = 0; j < maxLength; j++)
		tiling[i][i] = -1;
	int max = Iterate(0,0,tiling);
	std::cout << repr(tiling,max).str() << std::endl;
	return max+1;
    }
};
